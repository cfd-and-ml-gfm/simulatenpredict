#!/usr/bin/env python
# coding: utf-8

import numpy as np
import subprocess as sp
import os, sys
from numpy import savez_compressed
import pathlib  
import shutil  
import glob
from platform import python_version
  
dimCells = (117,86,38)
totalCells = np.prod(dimCells) 

def getEnabledCellsId():
    with open('building', 'r') as file:
        for i, line in enumerate(file):   
            if i == 18:
                meshCells = int(line.strip())
                break

    cellBoolArray = np.zeros((totalCells), dtype=bool)
    with open('building', 'r') as file:
        headerSize = 20
        line = file.readline()
        cnt = 1
        while line != '':  # The EOF char is an empty string
            #print(line, end='')
            line = file.readline()
            cnt += 1
            if (cnt > headerSize):
                value = int(line.strip())
                cellBoolArray[value] = True
                if (cnt == (meshCells + headerSize)):
                    break
    
    #Reshape building cells to C shape
    #buildingCells = cellBoolArray.reshape(dimCells,  order='F')
    #buildingCells = buildingCells.reshape(-1,  order='C')
    
    #buildingCells = cellBoolArray.reshape(-1,  order='C')
    buildingCells = cellBoolArray
    
    return buildingCells
  
if __name__ == "__main__":
    n_args = len(sys.argv)
    caseVar = 'U'
    if (n_args == 3):
        path = sys.argv[1]
        caseId = int(sys.argv[2])
        predictedTs = "ts%d%s.npz" % (caseId, caseVar)
    else:
        print("ERROR - USAGE: python %s <path> <ts>" % (sys.argv[0]))
        sys.exit(-1)

    print("Regenerating timestep %d in %s" % (caseId, path))
    
    ds = np.load(predictedTs)
    ds = ds.f.data
    #print(ds.flags['F_CONTIGUOUS'])
    #ds = np.ascontiguousarray(ds)
    #print(ds.flags['F_CONTIGUOUS'])
    print("Predicted DS shape", ds.shape)
 
    enabledCells = getEnabledCellsId()
    #print("Building DS shape", enabledCells.shape)

    cellsArray = []
    for idx, cell in enumerate(enabledCells):
        if cell:
            cellsArray.append(ds[idx])
          
          
    #ds = np.asfortranarray(cellsArray)
    ds = np.array(cellsArray)
    print("OpenFOAM DS shape", ds.shape)    
    
    filename = "%s/1/U" % (path)
    header = ""
    footer = ""
    values = "(\n"
    headerlines = 21 
    with open(filename, 'r') as reader:
        line = reader.readline()
        cntline = 0
        cntarray = 0
        while line:
            if cntline <= headerlines:
                header = header + line
                if cntline == headerlines:
                    ncells = int(line.strip())
                    reader.readline()
                    cntline += 1
            elif cntline > (headerlines + ncells +1):
                footer = footer + line
            else:
                cell = "(%f %f %f)\n" % (ds[cntarray][0], ds[cntarray][1], ds[cntarray][2])
                values = values + cell
                cntarray += 1
            line = reader.readline()
            cntline += 1
            
    dir1 = "%s/0" % (path)
    dir2 = "%s/%d" % (path, caseId)
    #print("Copying %s in %s ..." % (dir1, dir2))
    shutil.copytree(dir1, dir2, dirs_exist_ok = True)      
      
    filename = "%s/%d/U" % (path, caseId)      
    print("Writing file %s ..." % filename)
    with open(filename, 'w') as writer:     
        writer.write(header)
        writer.write(values)
        writer.write(footer)
    
    #print(values[:400]) 

    #UNCOMMENT!!! ONLY FOR TESTING PURPOSES
    os.remove(predictedTs) 