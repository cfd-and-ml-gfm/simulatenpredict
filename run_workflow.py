#!/usr/bin/env python
# coding: utf-8
import os
import sys
import shutil  
import subprocess as sp
from platform import python_version

def writeParamsFile(path, start, end):
    filename = path + "/params"
    print("Writing file %s ..." % filename)
    with open(filename, 'w') as f:
        f.write("ts_start\t%d;\n" % start)
        f.write("ts_end\t%d;\n" % end)

def createDirTree(srcpath, dstpath):
    if os.path.exists(dstpath) and os.path.isdir(dstpath):
        print("Removing directory %s ..." % (dstpath))
        shutil.rmtree(dstpath)

    print("Copying %s in %s ..." % (srcpath, dstpath))
    shutil.copytree(srcpath, dstpath, dirs_exist_ok=True)

if __name__ == "__main__":
    print('Notebook running on Python', python_version())   
    dimx = 117
    dimy = 86
    dimz = 38
    n_output = 10
    srcpath = 'CASO_PERFIL3_2'
    dstpath = "simulateNpredict2"
    npzfile = "test_input_dataset"
    n_input = 3
    
    print("----------------------------------------------------------------------")
    createDirTree(srcpath, dstpath)
    print("----------------------------------------------------------------------")
    
    print("----------------------------------------------------------------------")
    total_ts = 1
    while total_ts < 800:
        ts_start = total_ts
        ts_end = ts_start + n_input
        
        writeParamsFile(dstpath, ts_start, ts_end - 1)

        command = "simpleFoam -case %s" % dstpath
        print("----------------------------------------------------------------------")
        print("Running %s ..." % (command))
        sp.run(command.split(), capture_output=True) 
        print("----------------------------------------------------------------------")
        


        print("----------------------------------------------------------------------")
        command = "python 1-generate_dataset.py %s %s %d %d %d %d %d" % (dstpath, npzfile, ts_start, ts_end, dimx, dimy, dimz)
        #print("Running %s ..." % (command))
        #sp.run(command.split(), capture_output=False) 
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
        rc = child.returncode
        print(streamdata.decode())
        if(rc != 0):
            sys.exit()
        print("----------------------------------------------------------------------")
        
        

        print("----------------------------------------------------------------------")
        command = "python 4-CNN_prediction.py %s.npz %d %d %d %d %d" % (npzfile, ts_end, n_output, dimx, dimy, dimz)
        child = sp.Popen(command.split(), stdout=sp.PIPE)
        streamdata = child.communicate()[0]
        rc = child.returncode
        print(streamdata.decode())
        if(rc != 0):
            sys.exit()
        print("----------------------------------------------------------------------")
        
        
        print("----------------------------------------------------------------------")
        for ts in range(ts_end, ts_end + n_output):
            command = "python 3-regenerate_case.py %s %d" % (dstpath, ts)
            child = sp.Popen(command.split(), stdout=sp.PIPE)
            streamdata = child.communicate()[0]
            rc = child.returncode
            print(streamdata.decode())
            if(rc != 0):
                sys.exit()
        print("----------------------------------------------------------------------")
        
        
        print("----------------------------------------------------------------------")
        total_ts = total_ts + n_input + n_output
        print("\nTIMESTEP: %d\n" % total_ts)
        print("----------------------------------------------------------------------")
        
        #sys.exit()