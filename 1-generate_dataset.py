#!/usr/bin/env python
# coding: utf-8
import os
import sys
import numpy as np
import glob
import importlib
import pathlib
import sklearn
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import TruncatedSVD
from joblib import dump, load
from numpy import savez_compressed
from platform import python_version

#print('Notebook running on Python', python_version())
#print('Numpy version', np.version.version)
#print('Scikit-learn version {}'.format(sklearn.__version__))

from multiprocessing import Pool, Manager, cpu_count
m = Manager()
shared = m.list()

def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def mapValuesToCells(x):
    #print("Timestep: ", dictIds[x])
    scene = getTimestepData(x) 
    meshCells = scene.shape[0]
    cntEmpty = 0
    snapshot = np.array([0,0,0] * totalCells, dtype = np.float32).reshape((-1, 3), order='C') #C
    offset = 0   
    for i in range(totalCells):
        if (cellBoolArray[i]):
            snapshot[i] = scene[i-offset]
        else:
            offset+=1
    shared[dictIds[x]] = np.array(snapshot.reshape(dimCells + (3,),  order='F')) #F

def getTimestepData(ts):
    filename = "%s/%d/U" % (casename, ts)
    #print(filename)
    scene = []
    with open(filename) as reader:
        headerlines = 21
        head = [next(reader) for x in range(headerlines)]
        ncells = int(reader.readline().strip())
        reader.readline()
        for lineid in range(ncells):
            line = reader.readline()
            scene.append(list(line[1:-2].split(" ")))
    return np.array(scene, dtype = np.float32)
   
if __name__ == "__main__":
    error = False
    n_args = len(sys.argv)
    dimx = 117
    dimy = 86
    dimz = 38
    ts_start = 0
    ts_end = -1
    if (n_args == 1):
        input_path = '/home/amacias/CASOC_perfil3_prueba1' #ts-15-20
        output_path = "case3_ts-16-20"
    elif (n_args >= 5):
        input_path = sys.argv[1]
        output_path = sys.argv[2]
        ts_start = int(sys.argv[3])
        ts_end = int(sys.argv[4])
        if (n_args == 8):
            dimx = int(sys.argv[5])
            dimy = int(sys.argv[6])
            dimz = int(sys.argv[7])
        elif n_args != 5 and n_args > 8:
            error = True
    else:
        error = True
    
    dimCells = (dimx, dimy, dimz)
    if error:
        print("ERROR - USAGE: python %s <case_dir_path> <npz_file_path> <ts_start> <ts_end> <dimX> <dimY> <dimZ>" % (sys.argv[0]))
        sys.exit(-1)
    else:
        print("Reading case from \"%s\"[%d:%d] writing into \"%s\" with dimensions: " % (input_path, ts_start, ts_end, output_path), dimCells)
    
    # Define case dimensions
    #path = '/home/amacias/CASOC_SST2H' #case1
    #path = '/home/amacias/CASOC_perfil3' #case3 (experimental)
    #path = '/home/amacias/CASOC_perfil2' #case2
    #path = '/home/amacias/CASOC_perfil3_prueba1' #ts-15-20

    path = input_path
    totalCells = np.prod(dimCells) 
    cellBoolArray = np.zeros((totalCells), dtype=bool)
    dictIds = {}

    with open('building', 'r') as file:
        for i, line in enumerate(file):   
            if i == 18:
                meshCells = int(line.strip())
    
    # Create the boolean array that indicates the building positions
    with open('building', 'r') as file:
        headerSize = 20
        line = file.readline()
        cnt = 1
        while line != '':  # The EOF char is an empty string
            #print(line, end='')
            line = file.readline()
            cnt += 1
            if (cnt > headerSize):
                value = int(line.strip())
                cellBoolArray[value] = True #not a building cell
                if (cnt == (meshCells + headerSize)):
                    break
                    
    #print(cellBoolArray.shape[0], meshCells)                
    
    casename = path
    #for casename in glob.glob("case*"):
    
    # Get timesteps
    (dirpath, dirnames, filenames) = next(os.walk(casename))
    ids=[]
    for dir in dirnames:
        if (is_number(dir)):
            num = float(dir)
            if (num >= 0):
                ids.append(num)
    ids = sorted(ids)
    ids = ids[ts_start:ts_end+1]
    print("Processing timesteps: ", ids)
    
    # With this dictionary we prevent errors when timesteps do not start in 0 or they are not equally spaced in time.
    for idx, tsId in enumerate(ids):
        dictIds[tsId] = idx    
    #print(dictIds)

    # Map data from VTK to the snapshot
    for i in range(len(ids)):
        shared.append(-1)

    p = Pool(cpu_count())
    #p = Pool(1)
    p.map(mapValuesToCells, ids)
    p.close()
    p.join() 
    
    #scene = np.stack(shared[1:], axis=0)
    scene = np.stack(shared, axis=0)

    #casename = "case3_ts-16-20"
    casename = output_path
    #scene = scene[1:]
    
    print("Saving file: %s.npz, with shape:" % (casename), scene.shape)
    savez_compressed(casename, data=scene, header=None)
    #print("Done!")
    
    
    #COMMENT!!! ONLY FOR TESTING PURPOSES
    """
    #for idx, ts in enumerate(range(ts_start, ts_end)): 
    #print(scene[1][:30])
    tsU = scene[1].reshape((-1, 3), order='F')
    print(tsU[:10])
    path = "ts2U.npz"
    print("Saving file: %s, with shape:" % (path), tsU.shape)
    savez_compressed(path, data=tsU, header=None)   
    np.savetxt('ts2U.out', tsU, delimiter='\n') 
    """