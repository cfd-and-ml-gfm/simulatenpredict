import os,sys
from decimal import Decimal
import numpy as np
import glob
import importlib
import gc
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_absolute_error, mean_squared_error
import pickle
import gc # Garbage Collector
import pandas as pd
from numpy import savez_compressed
from joblib import dump, load

from keras.models import load_model
from keras.callbacks import ModelCheckpoint

import keras
import tensorflow as tf

class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_datasets, batch_size=128, dim=(32,32,32), n_channels=3, shuffle=True, observation_samples=3, multistep=1):
        #print('Generator Initialization')
        self.dim = dim
        self.batch_size = batch_size
        self.n_channels = n_channels
        self.shuffle = shuffle
        self.n_input = observation_samples      
        self.n_output = multistep
        self.last_samples = self.n_input - 1 + self.n_output      
        
        self.indexes = []
        cnt = 0
        self.total_samples = 0
        for case in list_datasets:
            samples = len(case) - self.last_samples
            self.total_samples += samples
            for sample in range(samples):
                self.indexes.append(cnt)
                cnt += 1
            cnt += self.last_samples
        
        self.ds = np.concatenate((list_datasets), axis = 0)
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(self.total_samples / self.batch_size))
  
    def __getitem__(self, batch_index):          
        'Generate one batch of data through dataset'
        list_IDs_temp = self.indexes[batch_index * self.batch_size : (batch_index+1) * self.batch_size]
        #print(index, list_IDs_temp)
        # Generate data
        X, y = self.__data_generation(list_IDs_temp)
        #print('Yieded batch %d' % index)
        return X, y

    def on_epoch_end(self):
        #'Updates indexes after each epoch'
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples'
        # Initialization
        X = np.empty((self.batch_size, *self.dim, self.n_channels * self.n_input))
        y = np.empty((self.batch_size, *self.dim, self.n_channels * self.n_output))
        
        for i, ID in enumerate(list_IDs_temp):  
            inFrom = ID
            inTo = inFrom + self.n_input
            outFrom = inTo
            outTo = outFrom + self.n_output
            
            X[i] = np.concatenate((self.ds[inFrom : inTo]), axis = 3)
            y[i] = np.concatenate((self.ds[outFrom : outTo]), axis = 3)
        return X, y
        

if __name__ == "__main__":
    error = False
    n_args = len(sys.argv)
    dimX = 117
    dimY = 86
    dimZ = 38
    n_output = 10
    n_input = 3
    if (n_args == 1):
        inputfile = "case3-first5"
        outputfile = "tsU"
    elif (n_args >= 4):
        inputfile = sys.argv[1]
        ts_start = int(sys.argv[2])
        n_output = int(sys.argv[3])
        if (n_args == 7):
            dimX = int(sys.argv[4])
            dimY = int(sys.argv[5])
            dimZ = int(sys.argv[6])
        elif n_args != 4 and n_args > 7:
            error = True
    else:
        error = True
    
    dims = (dimX, dimY, dimZ)
    n_cells = dimX * dimY * dimZ
    if error:
        print("ERROR - USAGE: python %s <input filename> <ts_start> <n_output> <dimX> <dimY> <dimZ>" % (sys.argv[0]))
        sys.exit(-1)
    else:
        print("From file \"%s\" prediction timesteps from %d to %d with dimensions: " % (inputfile, ts_start, ts_start + n_output), dims)
    
    
    n_channels = 3
    leakyrelu = lambda x: tf.keras.activations.relu(x, alpha=0.1)
    
    ds = []
    #filelist = ["case3-first5.npz"]
    filelist = [inputfile]
    for filename in filelist:
            print(filename)
            dsaux = np.load(filename)
            dsaux = dsaux.f.data
            dsaux = dsaux[:5]
            print(dsaux.shape)
            
            #path = "case3-first5"
            #print("Saving file: %s.npz, with shape:" % (path), dsaux.shape)
            #savez_compressed(path, data=dsaux, header=None)
            
            ds.append(dsaux.reshape(dsaux.shape[0] * dsaux.shape[1] * dsaux.shape[2] * dsaux.shape[3], dsaux.shape[4]))
            #print(ds[-1].shape)
    ds = np.array(ds)
    ds = np.concatenate(ds, axis=0)
    
    #print('Dataset size in memory: %0.2f GB' % (ds.nbytes / 1024**3))
    #np.info(ds)
    
    # ## Load model

    path = "CNN-multistep/"

    model = load_model(path + "model.h5", compile=True, custom_objects={'<lambda>': leakyrelu})
    #model.summary()
    model.load_weights(path + "train-06-0.03.hdf5")

    scalerX = load(path + 'scalerX.joblib')
    scalerY = load(path + 'scalerY.joblib')
    scalerZ = load(path + 'scalerZ.joblib')

    #print(ds.shape)
    dsX = ds[:, 0]
    dsX = dsX.reshape((-1, n_cells))
    dsY = ds[:, 1]
    dsY = dsY.reshape((-1, n_cells))
    dsZ = ds[:, 2]
    dsZ = dsZ.reshape((-1, n_cells))
    #print(dsX.shape)

    scalerX.fit_transform(dsX)
    dsX = dsX.reshape(-1, dimX, dimY, dimZ, 1)
    scalerY.fit_transform(dsY)
    dsY = dsY.reshape(-1, dimX, dimY, dimZ, 1)
    scalerZ.fit_transform(dsZ)
    dsZ = dsZ.reshape(-1, dimX, dimY, dimZ, 1)

    ds_test = np.concatenate((dsX, dsY, dsZ), axis=4)
    #print(ds_test.shape)

    # ## Prediction
    test = ds_test.reshape((1,) + dims + (-1,))
    pred_output = model.predict(test, verbose=1)
    pred_output = pred_output.reshape((n_output,) + dims + (n_channels,))
    #print(pred_output.shape)

    # ## Inverse scaling of test and prediction dataset

    test_output = ds_test

    #dsX_test = test_output[:,:,:,:,0].reshape(-1, n_cells)
    #scalerX.inverse_transform(dsX_test)
    dsX_pred = pred_output[:,:,:,:,0].reshape(-1, n_cells)
    scalerX.inverse_transform(dsX_pred)

    #dsY_test = test_output[:,:,:,:,1].reshape(-1, n_cells)
    #scalerY.inverse_transform(dsY_test)
    dsY_pred = pred_output[:,:,:,:,1].reshape(-1, n_cells)
    scalerY.inverse_transform(dsY_pred)

    #dsZ_test = test_output[:,:,:,:,2].reshape(-1, n_cells)
    #scalerZ.inverse_transform(dsZ_test)
    dsZ_pred = pred_output[:,:,:,:,2].reshape(-1, n_cells)
    scalerZ.inverse_transform(dsZ_pred)

    #print(dsX_test.shape)
    #print(dsX_pred.shape)
    
    dsX_pred = dsX_pred.reshape((n_output,) + dims + (1,))
    dsY_pred = dsY_pred.reshape((n_output,) + dims + (1,))
    dsZ_pred = dsZ_pred.reshape((n_output,) + dims + (1,))
    pred_output = np.concatenate((dsX_pred, dsY_pred, dsZ_pred), axis=4)
    
    print(test_output.shape)
    print(pred_output.shape)
     
    for idx, ts in enumerate(range(ts_start, ts_start + n_output)): 
        tsU = pred_output[idx].reshape(-1, n_channels)
        path = "ts%dU.npz" % (ts)
        print("Saving file: %s, with shape:" % (path), tsU.shape)
        savez_compressed(path, data=tsU, header=None)    